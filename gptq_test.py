from transformers import AutoTokenizer, pipeline, logging
from auto_gptq import AutoGPTQForCausalLM, BaseQuantizeConfig
import argparse

from json_responses import parse_json

model_name_or_path = "/home/future-leader/text-generation-webui/models/TheBloke_Nous-Hermes-13B-GPTQ"
model_basename = "nous-hermes-13b-GPTQ-4bit-128g.no-act.order"

use_triton = False

tokenizer = AutoTokenizer.from_pretrained(model_name_or_path, use_fast=True)

model = AutoGPTQForCausalLM.from_quantized(model_name_or_path,
        model_basename=model_basename,
        use_safetensors=True,
        trust_remote_code=True,
        #tee hee hee, we don't have a cuda, use a different device
        device="cpu",
        use_triton=use_triton,
        quantize_config=None)

print("\n\n*** Generate:")

prompt = ("I've built a large scale document search engine called Ambar- It implements OCR, translation, crawling, and "
          "many other methods to make it as easy as possible to search for relevant information in large aggregations "
          "of documents. Below I will list a series of introductory tasks meant to familiarize users with the "
          "application. I will also list a variety of comments provided in response to each task. Please use all of "
          "this information to write a beginner's guide for my application. ")

questions, answers = parse_json("ambar-responses.json")

QA_formatted = ""
for i, question in enumerate(questions[0:5]):
    QA_formatted += question + ":\n"
    answer_list = answers[i]
    for answer in answer_list:
        QA_formatted += "\t" + answer + "\n"
    QA_formatted += "\n"


prompt_template=f'''### Instruction: {prompt}
### Input: {QA_formatted}
### Assistant:'''

#changed cuda to cpu here
input_ids = tokenizer(prompt_template, return_tensors='pt').input_ids.cpu()

#need to change model.generate to specify type, or maybe tokenizer

output = model.generate(inputs=input_ids, max_new_tokens=512)
print(tokenizer.decode(output[0]))

# Inference can also be done using transformers' pipeline

# Prevent printing spurious transformers error when using pipeline with AutoGPTQ
logging.set_verbosity(logging.CRITICAL)

print("*** Pipeline:")
pipe = pipeline(
    "text-generation",
    model=model,
    tokenizer=tokenizer,
    max_new_tokens=512,
    temperature=0.7,
    top_p=0.95,
    repetition_penalty=1.15
)

print(pipe(prompt_template)[0]['generated_text'])
