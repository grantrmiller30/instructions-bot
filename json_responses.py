"""
Should be able to import this as long as it is in the same directory as the json responses.
Must call parse_string, and then questions can be queried for 1-33.
Responses is a list of dictionaries, each of which can be queried for 1-33.
"""
import simplejson as json


def parse_json(filename: str):
    responses = []
    with open(filename, 'r') as f:
        text = f.read()
    raw_dict = json.loads(text)
    responses_raw = raw_dict["responses"]
    questions = list(raw_dict["questions"].values())
    for i in range(1, 34):
        responses.append([group["q" + str(i)] for group in responses_raw if group["q" + str(i)]])

    return questions, responses


if __name__ == "__main__":
    questions, answers = parse_json("ambar-responses.json")
    QA_formatted = ""
    for i, question in enumerate(questions[0:10]):
        QA_formatted += question + ":\n"
        answer_list = answers[i]
        for answer in answer_list:
            QA_formatted += "\t" + answer + "\n"
        QA_formatted += "\n"
    print(QA_formatted)
